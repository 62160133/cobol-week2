       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BirthDate.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  BirthDate.
           02 YearOfBirth.
              03 CenturyOB         PIC 99.
              03 YearOB            PIC 99.
           02 MonthOfBirth         PIC 99.
           02 DayOfBirth           PIC 99.
       PROCEDURE DIVISION.
           MOVE 19750215  TO BirthDate 
           DISPLAY "MONTH IS = " MonthOfBirth 
           DISPLAY "CENTORY OF BIRTH IS = " CenturyOB 
           DISPLAY "YEAR OF BIRTH IS = " YearOfBirth
           DISPLAY DayOfBirth  "/" MonthOfBirth "/" YearOfBirth
           MOVE ZEROS TO YearOfBirth
           DISPLAY "BIRTH DATE = " BirthDate.
       END PROGRAM BirthDate.
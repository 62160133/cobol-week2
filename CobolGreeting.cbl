       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CobolGreeting.
      *>Program ti disploy COBOL greetings
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  ItemNum  PIC 9 VALUE 5.

       PROCEDURE DIVISION.
       BeginProgram.
           PERFORM  DisplayGreeting ItemNum TIMES .
           STOP RUN.

       DisplayGreeting.
           DISPLAY "Greetings from COBOL".
           